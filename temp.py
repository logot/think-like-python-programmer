# a_list = ["a", "b", "c", "d", "e", "f"]
# # b_list = a_list[0:1] + ["x", "y"] + a_list[3:]
# b_list = a_list[0:1] + a_list[3:]
# print(b_list)


# layout = "{0:>4}{1:>6}{2:>6}{3:>8}{4:>13}{5:>24}"
# print(layout.format("i", "i2", "i3", "i5", "i10", "i20"))
# for i in range(1, 11):
#     print(layout.format(i, i**2, i**3, i**5, i**10, i**20))

fruit = ["banana", "apple", "quince"]
fruit_2 = ["pear"] + fruit[1:]
fruit_3 = fruit[0:2] + ["orange"] # ['pear', 'apple', 'orange']

print(fruit_2)
print(fruit_3)
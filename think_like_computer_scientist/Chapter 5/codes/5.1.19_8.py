def mirror(str: str) -> str:
    return str + str[::-1]

mirror("good") == "gooddoog"
mirror("Python") == "PythonnohtyP"
mirror("") == ""
mirror("a") == "aa"

print(mirror("good"))
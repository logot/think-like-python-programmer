# https://www.journaldev.com/23584/python-slice-string

##
def reverse(string):
    index = -1
    while index >= -len(string):
        letter = string[index]
        print(letter, end='')
        index -= 1
    return ''

string = input('Enter a string: ')
print(reverse(string))

##
# def reverse(string):
#     return string[::-1]

# txt = "Hello World"
# print(reverse(txt))

# ##
# s = 'HelloWorld'
# reverse_str = s[2:6:1]
# print(reverse_str)
# reverse_str = s[2:6]
# print(reverse_str)
# reverse_str = s[2:6:2]
# print(reverse_str)
# reverse_str = s[2:6:-1]
# print(reverse_str) # nothing
# reverse_str = s[6:1:-1]
# print(reverse_str)
# reverse_str = s[6:1:-2]
# print(reverse_str)


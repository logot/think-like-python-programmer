# Computer Science with Git and Python

Hi, I am Jupeter, a member of **Team Jupeter**, Korean developers of [ABC Project](http://www.realtimekorea.net).

This repo is for the TEST of the [FAS Project](https://www.realtimekorea.net/fas), which is one of the ABC projects. **ABC 100T(= 1,000,000Box)** will be given to the contributors of this repo according to the amount of contributions by each. See [ABC](https://www.realtimekorea.net/abc) for detail.

## Course Introduction

This is the follow-up course of the EMC[Link](https://www.youtube.com/watch?v=uTVnzwGh5xQ&list=PLlSZlNj22M7TdA-tFwdWRAXQ2etAegMqs) course which is mainly for elementary students.

This course is composed of several subsidiary courses of which each uses an **Open Source** text book below:

1. [Ry's Git Tutorial](https://johnmathews.is/rys-git-tutorial.html)
2. [Think Like a Computer Scientist: Learning with Python 3](https://openbookproject.net/thinkcs/python/english3e/)
3. [The Hitchhiker’s Guide to Python!](https://docs.python-guide.org/)
4. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/2e/chapter1/)
5. [Python for Everybody](https://www.py4e.com/book.php)
6. [Python Data Science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/)
7. [Introduction to Machine Learning with Python](https://www.academia.edu/42736911/Introduction_to_Machine_Learning_with_Python_A_Guide_for_Data_Scientists)

Though those books are quite wonderful,
1. We are adding some addtional functionality on them to support learners via [FAS Project](https://www.realtimekorea.net/fas).
2. As we are **Functional** programmers. When appropriate, we add functional codes alternative to the sample codes in those books.
3. We are makining lectures for each book, and uploade them onto Youtube for freely available. Because the primary target learners are Korean students, the lectures are recorded in Korean. But, anyone can enjoy the lectures using Y**outube Auto-translation**.

## Contribute to this repo

### Authors and developers

To contribute,

- Write codes for the FAS system in this repo.
- Write the contents from Chapter 1 to 20.
- Add exercises to each chapter.
- Modify or bug fixes in contents and exercise solutions.
- Participate discussions on [RTK Discord Channel](https://discord.com/channels/932838137577750569/932838383426891797).

### Learners

To contribute,

- Fork the repository.
- Add a branch of which name is your GitLab ID.
- Make a file or directory for each chapter in the branch above.
- Write exercise solutions to the files above.
- Pull request your solutions.

## Let's have some fun

 Our first subsidiary course is on the renowned book **How to Think Like a Computer Scientist: Learning with Python 3, 3rd edition** was published on April, 2020, under GNU open source license.

  From the first edition of this book on 2002, it has enlightened many future and now present developers, but, it has a few shortages from the prospect of year 2022, at least for me. For example,

1. The grammar used in this book is mostly Python 3.5~3.8, but, at the time of writing this repo, the Python version is 3.10.0 which has a few significant changes.
2. Rather short of advanced concepts such as Iterators, Generators and Decorators.
3. Rather short of debugging techniques.
4. Rather short of memory architecture such as stack and heap.
5. Without static typing, which is just great both for commentary and debugging.

## Motivation

Though Python has been OOP language from its birth, it has gradually embraced functional programming techniques. At last, Python version 3.10.0 on October, 2021 has included a structural pattern matching, **Match Case**, which OOP developers are not accustomed to.

Though the books above are already quite good, but if we complement its current shortages, these books will be even more valuable for students learning Python Programming and current Python developers without functional programming experiences.

So, from December 1, 2021, I started to revise these books to use it as text books of named **Computer Science with Git and Python - Both OOP and Functional** mainly for Korean students from K5 to K12, that is elementary to high school students, and expect it to be used from March, 2022.

Though the intended readers were Korean students, but, as revised and newly added chapters of this book is being written in English, not only Koreans, but anyone can enjoy the book.

## Intension

As a member of **Team Jupeter**, my intention to revise these books is to:

1. Replace the grammar used in the book to those of Python 3.10.0+
2. Write functional version of sample codes and exercises in the book by using new Python 3.10.0 grammars and functional libraries such as **functools**.
3. Introduce **static typing** and explain memory architecture such as **stack and heap**.
4. Explain some advanced concept such as Generators.
5. Explain debugging and more detailed algorithm theory
6. Add static typing on code samples.

## Revision Process

  Beside source codes of the book here, the text of each chapter is stored and edited in Google Docs. Readers can see the on-going process in real time. The revision work is expected to complete March, 2022.

   The text of the original book is colored black, and the newly added parts are differently colored like;

```diff
- text in red
+ text in green
! text in orange
# text in gray
@@ text in purple (and bold)@@

```

## Table of Contents

The order of Chapter 1 to 15 are same to the original book with some added source codes and text. Chapter 16~20 are to be newly added.

## Readers

We assume the readers of our revised book have either (1) studied the **EMC** course made by **Team Jupeter(팀 주피터)**, **Scratch coding** for elementary students, or (2) some learning experience on programming languages like JavaScript.

Expected readers are:

1. K4 ~ K12 students, age from 10 to 18.
2. Have learned or are learning basic coding courses such as EMC of Team Jupeter
3. Have some English skills.
4. Current Python users without functional programming experiences.

## Youtube

  This being-revised book has accompanying lecture videos on Youtube in Korean(한국어) narration. However, not only Korean students, but also those not Korean speakers can freely watch the lectures using Youtube caption and auto translation.

  The Youtube playlist has about 200 episodes, of which each running time being about 10 minutes. For convenience, the playlist is divided into 4.

1. [Part 1](https://www.youtube.com/watch?v=XyjbAeIj4oA&list=PLlSZlNj22M7RjeCn-sYRHkns9j_gtc2tf): Episode 001~049 | Chapter 1 ~ 4
2. Part 2: Episode 050~099 | Chapter 5 ~ 8
3. Part 3: Episode 100~149 | Chapter 9 ~ 15
4. Part 4: Episode 150~200 | Chapter 16 ~ 20 - those are to-be newly added in this book version in January 2022.

- See the playlists of this book.
- See Youtube [full playlists](https://www.youtube.com/channel/UCxnsWjMKyb6px5lDiqInDHA/playlists) of Team Jupeter.

## Further Learnings

These revised books and accompanying lectures, titled as **Computer Science with Git and Python**, is a part of a larger learning courses provided by Team Jupeter(all lectures are in **Korean** narration), consist of about 7,000 Youtube video clips/episodes:

All lectures are freely available via Youtube. Some of them are;

- Elementary(K1~K6) Course:
  - EMC | Basic - Learning Scratch Coding
  - EMC | Intermediate - Learning Scratch Coding
  - EMC | Advanced - Learning App Inventor
  - Middle School(K7~K9):
  - Python and Functional Programming
  - Gleam Programming Language

- Secondary School(K7~K12):
  - JS/TS Web Development - HTML, CSS, JavaScript/TypeScript, React.js, Vue.js, Svelte, Angular and Cycle.js
  - Rust & System Programming | Intermediate
  - Python & Data Science
  - Elixir & Phoenix Web Development | Basic

- College
  - Rust & System Programming | Advanced
  - Elixir & Phoenix Web Development | Advanced
  - Python & Machine Learning

## Recommended courses, learning in order

```mermaid
graph LR
A[초등학교] ---> B[중학교] -------> C[고등학교] ------> D[대학]
```

```mermaid
graph LR
A[EMC] --> B[Computer Science with Git & Python]
A --> C[Web Components]
C --> R[Web Frameworks - React, Vue, Angular, Svelte, Cycle]
A --> H[Rust - Basic]
H --> I[Rust - Intermediate]
I --> J[Rust - Advanced]
C --> D[Elixir & Phoenix - Basic]
D --> K[Elixir & Phoenix - Advanced]
B --> E[Python Data Science]
B --> G[Advanced Git]
E --> F[Machine Learning]
A --> L[Cloud - Basic]
L --> M[Cloud - Intermediate]
M --> N[Cloud - Advanced]
A --> O[Linux - Basic]
O --> P[Linux - Intermediate]
P --> Q[Linux - Advanced]

```
